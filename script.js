var main = document.querySelector("main");
var login = document.getElementById("login");
var chat = document.getElementById("chat");
var channel = autojoin;
var serverKnown = false;
var server;
var nick;
var pass;

// Let's set <title> and greetings line.
document.querySelector("title").innerText = channel;
document.getElementById("greetings").innerText = greetings;
var websocket = new WebSocket(url);
websocket.onerror = function() {
  main.innerHTML = "<br>" +
      "<span>Error: can not connect to websocket server " + url + "</span>";
}
websocket.onclose = function() {
  chat.querySelector("div").innerHTML += "<br>" +
      "<span>Error: connection closed. Refresh page to reconnect.</span>";
}

// Start main actions.
websocket.onopen = function() {
  askForLogin();
}

function pinger() {
  setTimeout(function(){
    websocket.send("PING " + server);
    console.log("sending PING " + server);
    pinger();
  }, 90000);
}

///// Login functions.

function askForLogin() {
  var nickform = addLoginForm("nickname");
  nickform.onsubmit = function(event) {
    event.preventDefault();
    if (nickform.elements.nickname.value === "") {
      login.innerHTML += "<div><span>Nickname required.</span></div>";
      askForLogin();
    }
    else {
      nickform.elements.nickname.readOnly = true;
      nick = nickform.elements.nickname.value;
      var passform = addLoginForm("password");
      passform.elements.password.type = "password";
      passform.onsubmit = function(event) {
        event.preventDefault();
        pass = passform.elements.password.value;
	passform.elements.password.value = "";
	startChat();  // Chat starts here.
	pinger();
      }
    }
  }
}

function addLoginForm(name) {
  var form = document.createElement('form');
  var label = document.createElement('label');
  label.for = name;
  label.innerText = name[0].toUpperCase() + name.slice(1) + ": ";
  var input = document.createElement('input');
  input.id = name;
  form.appendChild(label);
  form.appendChild(input);
  login.appendChild(form);
  input.focus();
  return form;
}

///// Chat functions.

function startChat() {
  if (pass !== "")
    websocket.send("PASS " + pass);
  pass = "";
  websocket.send("NICK " + nick);
  websocket.send("USER " + nick + " hostname" + " servername" + " realname");
  login.remove();
  chat.style.display = "block";
  chatLogger();
  msgSender();
}

function chatLogger() {
  var space, message;
  var chatlog = document.createElement('div');
  chatlog.innerHTML = '<br>';
  chat.appendChild(chatlog);
  websocket.onmessage = function(msg) {
    console.log(msg.data);
    if ((message = msgProc(msg.data)) !== "") {
      space = chatlog.scrollHeight - chatlog.scrollTop - chatlog.clientHeight;
      chatlog.innerHTML += "<span>" + message + "\n</span>";
      if (space < 15)
        chatlog.scrollTop = chatlog.scrollHeight - chatlog.clientHeight;
    }
  }
}

function msgSender() {
  var form = document.createElement('form');
  var label = document.createElement('label');
  var textarea = document.createElement('textarea');
  var chatlog = chat.querySelector("div");
  var command = "";
  var params = "";
  var string, i;
  form.appendChild(label);
  label.innerText = nick + ": ";
  form.appendChild(textarea);
  chat.appendChild(form);
  textarea.focus();
  textarea.addEventListener("keyup", function(event) {
    if (textarea.value.charAt(textarea.value.length - 1) === '\n') {
      string = textarea.value;
      textarea.value = "";
      if (string[0] === '/') {
	for (i = 1; string[i] !== ' '&& string[i] !== '\n'; i++)
	  command += string[i];
	if (string[i] === ' ')
	  i++;
	while (string[i] !== '\n')
	  params += string[i++];
	command = command.toUpperCase();
	if (command === "LIST")
	  chat.querySelector("div").innerHTML += "<br>" +
	      "<span>Sorry, LIST command isn't implemented yet</span>";
	else
	  websocket.send(command + ' ' + params + '\n');
        command = "";
        params = "";
      }
      else if (channel !== "") {
        websocket.send("PRIVMSG " + channel + " :" + string);
        var space = chatlog.scrollHeight - chatlog.scrollTop - chatlog.clientHeight;
	chatlog.innerHTML += "<span>" + nick + ": " + string + "</span>";
        if (space < 15)
          chatlog.scrollTop = chatlog.scrollHeight - chatlog.clientHeight;
      }
      else
	websocket.send(string);
    }
  });
}

function msgProc(message) {
  var i = 0;
  var prefix = "";
  var command = "";
  var middle = "";
  var trailing = "";
  var user = "";
  var result = "";
  var dateh = new Date().getHours();
  var datem = new Date().getMinutes();

  if (message[0] == '@')  // ignore TAGMSG for now
    return "";

  if (message[0] == ':') {
    for (i = 1; message[i] !== ' ' && (message[i]); i++)
      prefix += message[i];
    if (message[i] === ' ')
      i++;
  }
  while (message[i] !== ' ' && (message[i]))
    command += message[i++];
  if (message[i] === ' ')
    i++;
  if (message[i] !== ':') {
    while (message[i] !== ':' && (message[i]))
      middle += message[i++];
    if (message[i] === ' ')
      i++;
  }
  if (message[i] == ':') {
    for (i++; (message[i]); i++)
      trailing += message[i];
  }

  if (!serverKnown && RegExp('\\.').test(prefix)) {
    server = prefix;
    serverKnown = true;
  }

  for (i = 0; prefix[i] !== '!' && prefix[i] !== '@' && (prefix[i]); i++)
    user += prefix[i];

  switch (command) {
    case "001":
    case "002":
    case "003":
    case "004":
    case "005":
    case "250":
    case "251":
    case "252":
    case "254":
    case "255":
    case "265":
    case "266":        // server service messages
    case "333":        // topic author and creation date
    case "PONG":
      break;
    case "376":        // motd end
      if (channel !== "")
        websocket.send("JOIN " + channel);            //// CHANNEL AUTOJOIN
    case "375":        // motd start
    case "706":        // end of help message
    case "366":        // end of names list
    case "374":        // end of info
      result = "=====";
      break;
    case "353":        // list of names in channel
      break;
    case "433":        // nickname is already in use
    case "704":        // help message header
    case "705":        // help message body
    case "372":        // motd body
    case "332":        // topic message
    case "371":        // info messages
    case "461":        // not enough parameters
    case "NOTICE":
      result = trailing;
      break;
    case "PING":
      if (middle === "") {
	websocket.send("PONG " + trailing);
	console.log("PONG " + trailing);
      }
      else {
        websocket.send("PONG " + middle);
	console.log("PONG " + middle);
      }
      break;
    case "PRIVMSG":
      result = '[' + (dateh < 10 ? '0' + dateh : dateh) + ':' +
	  (datem < 10 ? '0' + datem : datem) + '] ' + user + " >> " + trailing;
      break;
    case "NICK":
      if (user === nick) {
        nick = trailing;
	result = "You changed Nickname to " + nick + ".";
	chat.querySelector("label").innerText = nick + ": ";
      }
      break;
    case "JOIN":
      if (user === nick) {
	result = "You joined channel " + middle + ".";
	channel = middle;
        document.querySelector("title").innerText = channel;
      }
      break;
    case "PART":
      if (user === nick)
	result = "You leaved channel " + middle + ".";
      break;
    case "QUIT":
      if (user === nick)
	result = "You disconnected from IRC server."
      break;
    default:
      result = message;
      break;
  }
  return result;
}
